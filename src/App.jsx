import React, { Component } from 'react';
import './App.css';
import Layout from './components/layout';
import InfoSection from './components/infoSection'
import { Typography, Paper, Card, Grid, Button, CardContent, Select, MenuItem } from '@material-ui/core';
import SwLogo from './svg/sw.svg'
import * as axios from 'axios';
import { randomIntFromInterval } from './utils/random';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasLoaded: false,
      selected: 0,
      planets: []
    }

    this.addPlanet = this.addPlanet.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onClearButton = this.onClearButton.bind(this);
  }
  
  
  componentDidMount() {
    const planets = localStorage.getItem("planets")
    if (planets) {
      try {
        const planetsArray = JSON.parse(planets);
        this.setState({
          planets: planetsArray,
        })
      } catch (e) {

      }
    } else {
      localStorage.setItem("planets", "")
    }
    this.setState({ hasLoaded: true })
  }

  async addPlanet(number) {
    const n = randomIntFromInterval()
  number = typeof number === "number"? number: n
    const url = `https://swapi.co/api/planets/${number}/`
    try {
      const response = await axios.get(url)
      let planet = response.data
    try {
      this.setState({ hasLoaded: false })
      const films  = await this.grabFilms(planet.films)
      let planets = [...this.state.planets];
      planet.films = films;
      planets.push(planet);
      this.setState({ planets: planets })
      localStorage.setItem('planets', JSON.stringify(this.state.planets));
      this.setState({ hasLoaded: true, selected: this.state.planets.length-1 })
      console.log("Hun")
    } catch(error) {
      console.log("Error with the request(the server api is probably down)");
      console.log(error);
      this.setState({hasLoaded: true});
    }

    } catch (error) {
      console.log("Error with the request");
    }


  }

  async grabFilms(films) {
    let filmsArray = []
    if (Array.isArray(films)) {
        
        if (films.length > 0) {
            
            await Promise.all(
                films.map( async filmUrl => {

                    try {
                        const filmResponse = await axios.get(filmUrl);
                        filmsArray.push(filmResponse.data);
                    } catch(e) {
                        console.log(e)

                    }
                })
            )
        }
    }
    return filmsArray

} 
  render() {
    let planet = this.state.planets.length >= 1 ? this.state.planets[this.state.selected]
      : null
    return (
      <Layout>
        <Grid item justify="center" container xs={12} >
          <img src={SwLogo} width="80%" alt="" style={{marginTop: "2%"}} />
        </Grid>
        <Grid item container xs={12} spacing={6} justify="center" alignItems="center">
          <Grid
            item
            xs={12}
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: "center"
            }}
          >
            <Grid item xs={12} >
              <Typography align="center" >Random Planet Selector</Typography>
              <Button disabled={!this.state.hasLoaded}
                onClick={this.addPlanet}
                variant="contained"
                fullWidth
                color="primary" >{this.state.hasLoaded ? "Get a random planet" : "Loading..."}</Button>

            </Grid>
            
          </Grid>
          <Grid container 
          alignContent="center"
          justify="center"
          item spacing={6} xs={12}
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: "center"
          }}
          >
            
          {
              this.state.planets.length > 1? this.renderSelector() : null
            }
          
            {
              planet ? <InfoSection planet={planet} /> : this.renderNoPlanet()
            }
            
          </Grid>


          
        </Grid>
        
      </Layout>
      
    )
  }

  handleChange(event) {
    this.setState({selected: event.target.value})
  }

  renderSelector() {
    return (
        
          <Grid item container spacing={4} alignItems="center">
            
            <Paper>
              <Card>
                <CardContent>
                  <Grid item container spacing={4} justify="space-around">
                    <Grid item>
                  <Typography variant="h6">History</Typography>
            <Select name="selectPrevious" value={this.state.selected} fullWidth onChange={this.handleChange}>
              {
                this.state.planets.map( (planet, i) => {
                  return (
                    <MenuItem key={i} value={i}>{planet.name}</MenuItem>
                  )
                })
              }
            </Select>
                  </Grid>
                  <Grid item>
                  <Button onClick={this.onClearButton} variant="contained">Clear history</Button>
                  </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </Paper>
          </Grid>
        

    )
  }

  renderNoPlanet() {
    return (
      <Grid item>
        <Paper>
          <Card>
            <CardContent>
              <Typography align="center" >Hit the generate button</Typography>
            </CardContent>
          </Card>
        </Paper>
      </Grid>
    )
  }

  onClearButton(){
    this.setState({planets: []})
    localStorage.setItem("planets", "")
  }
}

export default App;