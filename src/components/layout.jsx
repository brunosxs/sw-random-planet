import React, { Component } from 'react';
import { Grid, Container, } from '@material-ui/core'
class Layout extends Component {
    render() {
        return (
            <Container>
                    <Grid item container md={12} spacing={2} justify="center">
                        {this.props.children}
                        </Grid>
                    
            </Container>
        );
    }
}

export default Layout;